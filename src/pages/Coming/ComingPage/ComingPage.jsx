import React from 'react';
import './Coming.scss';
import ComingGears from "../components/ComingGears";

const ComingPage = () => {
    return(
        <section className="coming">
            <div className="coming-container">
                <h1 className="coming-container-header">В данный момент страница находится в разработке</h1>
                <p className="coming-container-text">Возвращайтесь позже</p>
                <ComingGears/>
            </div>
        </section>
    )
}

export default ComingPage;
