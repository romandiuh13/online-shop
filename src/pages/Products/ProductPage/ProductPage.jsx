import React, {useEffect} from "react";
import './ProductPage.scss'
import ProductView from "../components/ProductView";
import ProductInfo from "../components/ProductInfo";
import {useSelector} from "react-redux";
import {actionOneProductGet} from "../../../app/store/Products/actions/actionOneProduct";
import {useDispatch} from "react-redux";
import Preloader from "../../../components/Preloader/Preloader";

const ProductPage = ({match}) => {
    const dispatch = useDispatch();
    const {params: {productName}} = match
    const loading = useSelector(state => state.allProduct.loading);
    const preloadingStyle = loading ? {display: 'block'} : {display: 'none'};

    useEffect(() => {
        const action = actionOneProductGet(productName);
        dispatch(action, productName)
    }, [productName])

    const oneProduct = useSelector(state => state.allProduct.oneProduct)

    return (
            <>
                <Preloader style={preloadingStyle} classNameContainer='categoryPreloaderStyle' classNameElement='preloader-5' />
                <div className='product'>
                    <div className={'product-name'}>
                        <p>{oneProduct.name}</p>
                    </div>
                    <ProductView images={oneProduct.imageUrls}/>
                    <ProductInfo product={oneProduct}/>
                </div>
            </>
        );
}

export default ProductPage
