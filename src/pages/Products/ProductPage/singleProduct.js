export const product = [
    {
        "enabled": true,
        "imageUrls": [
            "img/Products/men/001.png",
            "img/Products/men/002.png",
            "img/Products/men/003.png",
            "img/Products/men/004.png"
        ],
        "quantity": 100,
        "_id": "5fb503fa84090900174ec8bd",
        "name": "new product for testing purposes",
        "currentPrice": 199.99,
        "previousPrice": 250,
        "categories": "men",
        "color": "red",
        "productUrl": "/men",
        "brand": "braaaand",
        "myCustomParam": "some string or json for custom param",
        "itemNo": "314124",
        "date": "2020-11-18T11:22:34.901Z",
        "__v": 0
    }
]
