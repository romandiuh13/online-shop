import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {productsRequestCreator} from "../../../app/store/Products/actions/actionCreators";
import './CategoryProductPage.scss'
import AllProducts from "../components/AllProducts";
import FilterProducts from "../../../components/FilterProducts/FilterProducts";
import Preloader from "../../../components/Preloader/Preloader";
import AdaptiveFilterProducts from "../../../components/FilterProducts/component/AdaptiveFilterProducts";

const CategoryProductPage = ({match}) => {
    const [state, setState] = useState(false)
    const dispatch = useDispatch();
    const loading = useSelector(state => state.allProduct.loading);
    const preloadingStyle = loading ? {display: 'block'} : {display: 'none'};
    useEffect(() => {
        const action = productsRequestCreator();
        dispatch(action)
    }, [])
    const {params: {category},} = match;

    return (
        <>
            <div className='allProductsContainer'>
                {/*<button onClick={() => setState(true)} className='buttonFilter'>Фильтр</button>*/}
                <AdaptiveFilterProducts className='buttonFilter' />
                <FilterProducts  />
                <div className='category-container'>
                    <Preloader style={preloadingStyle} classNameContainer='categoryPreloaderStyle' classNameElement='preloader-5'/>
                    {/*{messageNull}*/}
                    <AllProducts category={category}/>
                </div>
            </div>

        </>
    )
}

export default CategoryProductPage
