import React from "react";
import './ProductView.scss'
import {useDispatch, useSelector} from "react-redux";
import {setActiveImage} from "./productViewReducer/setActiveImage";
import {deleteActiveImage} from "./productViewReducer/deleteActiveImage";

const ProductView = ({images}) => {

    const dispatch = useDispatch()
    const activeImage = useSelector(state => state.activeImage.activeImage)
    if (images) {
        const img = images.find(elem => elem === activeImage)
        if (!activeImage) {
            dispatch(setActiveImage(images[0]))
        }else if(!img){
            dispatch(deleteActiveImage())
        }
        const imageList = images.map((item) => (
            <div className='product-view-images-container' onClick={() => {
                dispatch(setActiveImage(item))
            }}>
                <img className='product-view-images-item' alt='asd' src={item}/>
            </div>
        ))
        return (
            <div className='product-view'>
                <div className='product-view-images'>{imageList}</div>
                <div className='product-view-active'>
                    <img className='product-view-images-item' alt='asd'
                         src={activeImage}/>
                </div>
            </div>
        )
    } else {
        return null
    }
}


export default ProductView
