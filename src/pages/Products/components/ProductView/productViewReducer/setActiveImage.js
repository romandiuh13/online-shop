import {ACTIVE_IMAGE} from "./constants";

export const setActiveImage = (payload) => {
    return{
        type: ACTIVE_IMAGE,
        payload
    }
}
