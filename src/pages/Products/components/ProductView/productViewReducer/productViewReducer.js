import {ACTIVE_IMAGE, DELETE_ACTIVE_IMAGE} from './constants'

export const productViewReducer = (state = {}, {type, payload}) =>{
    switch (type) {
        case  ACTIVE_IMAGE :
            return(
                {...state, activeImage: payload}
            )
        case  DELETE_ACTIVE_IMAGE :
            return(
                {...state, activeImage: null}
            )
        default : return state
    }
}
