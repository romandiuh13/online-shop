import React, {useState} from 'react';
import './ProductInfo.scss'
import '../../../../components/Button/Button'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faShoppingCart} from "@fortawesome/free-solid-svg-icons";
// import Button from "../../../../components/Button/Button";
import Quantity from "../../../../components/Quantity";
// import {minusItem} from "../../../Cart/modalAuthReducer/actions/minusItemAction";
// import {plusItem} from "../../../Cart/modalAuthReducer/actions/plusItemAction";
import {useDispatch} from "react-redux";
import {addCart} from "../../../../app/store/Cart/actions/addCartAction";
// import {closeModal} from "../../../../components/Modal/modalAuthReducer/closeModal";


const ProductInfo = ({product}) => {
    const dispatch = useDispatch()
    const [count, setCount] = useState(1)
    const addProductFromPage = () =>{
        setCount(0)
        product.counter= count;
        dispatch(addCart(product))
    };
    if (product) {
        return (
            <div className='product-info'>
                <div className='product-info-heading'>
                    <span
                        className='product-info-heading-price'>{product.currentPrice}</span>
                    <span
                        className='product-info-heading-number'>{product.itemNo}</span>
                </div>
                <div className='product-info-description'>
                    <span>{product.myCustomParam}</span>
                </div>
                <div className='product-info-size'>
                    <p className='product-info-size-heading'>Габарити</p>
                    <span>{product.myCustomParam}</span>
                </div>
                <div className='product-info-options'>
                    <div className='product-info-options-quantity'>
                        <span>Кількість</span>
                        <Quantity counter={count}
                                  onClickMinus={()=>setCount(count-1)}
                                  onClickPlus={()=>setCount(count+1)}/>
                    </div>
                    <div className='product-info-options-basket'>
                        <button onClick={()=> addProductFromPage()}
                            className='product-info-options-basket-button'>
                            У кошик
                            <FontAwesomeIcon icon={faShoppingCart}/>
                        </button>
                    </div>
                </div>
            </div>
        );
    }
};

export default ProductInfo;
