import React from "react";
import Button from "../../../../components/Button/Button";
import './cradProductStyle.scss'
import {NavLink} from "react-router-dom";

const CardProduct = ({ src, text, name, price, itemNo,categories,productUrl, alt='name photo', flag, onClickToProduct,onclickToCart}) =>{
    const style = {
        display: flag ? 'block' : 'none'
    }

    return(

        <div className='cardContainer' >
            <div className='containerImageCard'>
                <NavLink to={`/products/${categories}/${itemNo}`}>
                    <img onClick={onClickToProduct} className='productImage' src={src} alt={alt}/></NavLink>
            </div>
            <p className='cardProduct-name'>{name}</p>
            <p className='price'>{price}</p>
            <Button onClick={onclickToCart} style={style}  className='cardProduct-button' text={text}/>
        </div>
    )
}
export default CardProduct
