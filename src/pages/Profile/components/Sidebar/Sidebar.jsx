import React from "react";
import './Sidebar.scss'
import {v4 as uuidv4} from "uuid";
import {useDispatch, useSelector} from "react-redux";
import {removeDataUser} from "../../../../app/store/Auth/actions/removeDataUser";
import Button from "../../../../components/Button/Button";

const Sidebar = ({tabItems}) => {
    const dispatch = useDispatch();
    const state = useSelector(state => state.addNewCustomer.user);
    const dataUser = () => {
        dispatch(removeDataUser({}));
        localStorage.removeItem('customer')
    }
    return (
        <div key={uuidv4()} className='user-setting'>
            {tabItems}
            <Button onClick={dataUser} text='Выйти' />
        </div>
    )
}

export default Sidebar;
