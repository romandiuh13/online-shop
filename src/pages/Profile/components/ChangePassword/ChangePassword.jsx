import React, {useState} from "react";
import './ChangePassword.scss'
import {changePasswordFields} from "./changePasswordFields";
import Input from "../../../../components/Input/Input";
import axios from "axios";


const ChangePassword = () => {
    const [inputValue, setInputValue] = useState({});
    const token = localStorage.getItem('token');
    const handleChange = (event) => {
        const {name, value} = event.target
        inputValue[name] = value;
        setInputValue(inputValue);
    };
    const handleSubmit = (event) =>{
        event.preventDefault();

        const sendRequest = async () =>{
            try {
                const resp = await axios.put('https://glacial-river-19645.herokuapp.com/api/customers/password', inputValue,
                    {headers: {'Authorization': JSON.parse(token) }})
                setInputValue({});
                console.log(resp.data)
                alert('Ваш пароль был успешно изменен')

            } catch (error){
                console.dir(error)
                alert(error.response.data.message && error.response)
            }
        }
        return sendRequest();
    }


    return (
        <div className='user-change'>
            <h2 className='user-change-label'>Введите данные для изменения Вашего пароля</h2>
             <form onSubmit={handleSubmit}>
                 <Input {...changePasswordFields.password} onChange={handleChange} />
                 <Input {...changePasswordFields.newPassword} onChange={handleChange} />
                 <Input {...changePasswordFields.submit} />
             </form>

        </div>
    )
}

export default ChangePassword
