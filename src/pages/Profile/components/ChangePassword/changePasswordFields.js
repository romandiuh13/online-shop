export const changePasswordFields = {
    password: {
        classNameLabel: 'authLabel',
        type: 'password',
        placeholder: 'Старый пароль',
        cName: 'registerForm-container',
        name: 'password',
        label: 'password',
        text: 'Введите ваш старый пароль',
        minLength: 7,
        maxLength: 30,
        required: true
    },
    newPassword: {
        classNameLabel: 'authLabel',
        type: 'password',
        placeholder: 'Новый пароль',
        cName: 'registerForm-container',
        name: 'newPassword',
        label: 'newPassword',
        text: 'Придумайте новый пароль',
        minLength: 7,
        maxLength: 30,
        required: true
    },
    submit: {
        type: 'submit',
        value: 'Изменить',
        name: 'submit',
        cName: 'submit-btn'
    },
}
