export const userProfileFields = {
    firstName: {
        classNameLabel: 'EditLabel',
        type: 'text',
        placeholder: 'firstName',
        cName: 'editFrom-user',
        name: 'firstName',
        label: "FirstName",
        text: 'firstName',
    },
    lastName: {
        classNameLabel: 'EditLabel',
        type: 'text',
        placeholder: 'lastName',
        cName: 'editFrom-user',
        name: 'lastName',
        label: "lastName",
        text: 'lastName',
    },
    login: {
        classNameLabel: 'EditLabel',
        type: 'text',
        placeholder: 'login',
        cName: 'editFrom-user',
        name: 'login',
        label: "login",
        text: 'login',
    },
    email: {
        classNameLabel: 'EditLabel',
        type: 'email',
        placeholder: 'email',
        cName: 'editFrom-user',
        name: 'email',
        label: 'Email',
        text: 'Email',
    },
    submit: {
        type: 'submit',
        value: 'Изменить',
        name: 'submit',
        cName: 'submit-btn'
    },
}
