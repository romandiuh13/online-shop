import React from "react";
import UserProfile from "../UserProfile";
import ChangePassword from "../ChangePassword";
// import MyOrders from "../MyOrders";
import LogOut from "../LogOut";

const ContentPage = ({activeTab}) => {

    if (activeTab) {
        const {name} = activeTab
        return (
            <>
                {name === 'UserProfile' ? <UserProfile/> : null}
                {name === 'ChangePassword' ? <ChangePassword/> : null}
                {/*{name === 'MyOrders' ? <MyOrders/> : null}*/}
                {/*{name === 'LogOut' ? <LogOut/> : null}*/}
            </>
        )
    }else{
        return null
    }
}

export default ContentPage
