export const tabProps = [
    {
        name: "UserProfile",
        label: "Редактировать профиль",
        active: false,
        className: 'user-setting-Navbar active'
    },
    {
        name: "ChangePassword",
        label: "Изменить пароль",
        active: false,
        className: 'user-setting-Navbar'
    },
    // {
    //     name: "LogOut",
    //     label: "Выйти из кабинета",
    //     active: false,
    //     className: 'user-setting-Navbar'
    // }
]
