import React from 'react';
import './OrderPage.scss'
import OrderData from "../components/OrderData";
import OrderCart from "../components/OrderCart";
import {useSelector} from "react-redux";

const OrderPage = () => {
    const cart = useSelector(state => state.cart.cartList)
    return (
        <section className={'order'}>
           <OrderData/>
           <OrderCart cart={cart}/>
        </section>
    );
};

export default OrderPage;
