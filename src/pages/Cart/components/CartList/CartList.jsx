import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";
import Quantity from "../../../../components/Quantity";
import {removeCart} from "../../../../app/store/Cart/actions/removeCartAction";
import {minusItem} from "../../../../app/store/Cart/actions/minusItemAction";
import {plusItem} from "../../../../app/store/Cart/actions/plusItemAction";
import {useDispatch} from "react-redux";

const CartList = ({cart}) => {
    const dispatch = useDispatch()
    if (cart) {
        const itemsList = cart.map((item) => {
            const {imageUrls, name, itemNo, totalPrice} = item
            const counter = item.counter
            return (
                <>
                <div className={'cart-list'}>
                    <div className={'cart-list-view'}>
                        <FontAwesomeIcon className='cart-list-view-close' icon={faTimes} onClick={() => {dispatch(removeCart(item))}}/>
                        <div className={'cart-list-view-frame'}><img src={imageUrls[0]} alt={name} className={'cart-list-view-img'}/></div>
                        <div className={'cart-list-view-name'}>
                        <span className={'cart-list-view-name-label'}>{name}</span>
                        <span className={'cart-list-view-name-itemNo'}>{itemNo}</span>
                        </div>
                    </div>
                    <div className={'cart-list-quantity'}>
                        <Quantity counter={counter}
                                  onClickMinus={()=>dispatch(minusItem(item))}
                                  onClickPlus={()=>dispatch(plusItem(item))}/>
                    </div>
                    <span className={'cart-list-price'}>{totalPrice}</span>
                </div>

                </>
            )
        })
        return (
            <>
            {itemsList}
            </>
        );
    } else {
        return null
    }
};

export default CartList;
