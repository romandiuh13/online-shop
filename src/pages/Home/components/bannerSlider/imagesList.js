import banner1 from '../../../../assets/imagesSlider/banner1.png'
import banner2 from '../../../../assets/imagesSlider/banner2.png'
import banner3 from '../../../../assets/imagesSlider/image1.png'
import banner4 from '../../../../assets/imagesSlider/Screenshot.png'
import banner5 from '../../../../assets/imagesSlider/Lori-Elis.png'


export const imagesList = [
    {
        src: banner1,
        title: 'Комфорт для счастливых людей!',
        text: 'Скидка до 25%',
        descriptions: 'на все дубовые кресла',
        id: 1
    },
    {
        src: banner2,
        id: 2
    },
    {
        src: banner3,

        id: 3
    },
    {
        src: banner4,

        id: 4
    },
    {
        src: banner5,

        id: 5
    }
    ];
