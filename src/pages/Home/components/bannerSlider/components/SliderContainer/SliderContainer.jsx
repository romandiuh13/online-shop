import React from "react";
import Button from "../../../../../../components/Button/Button";

const SliderContainer = (props) => {
    const {classNameTitle, classNameText, classNameDescriptions, classNameButton, className, title, src, text, descriptions, buttonText, flag, sliderImgStyle} = props;
    const style = {
        display: flag ? 'block' : 'none'
    }
    return(
        <div className={className}>
            <h3 className={classNameTitle}>{title}</h3>
                <p className={classNameText}>{text}</p>
                <p className={classNameDescriptions}>{descriptions}</p>
                <Button style={style} className={classNameButton} buttonText={buttonText}/>
                <img className={sliderImgStyle} src={src} alt={text}/>
        </div>
    )
};

export default SliderContainer
