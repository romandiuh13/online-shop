import React from "react";
import {useSelector} from "react-redux";
import Preloader from "../../../components/Preloader/Preloader";
import PopularProducts from "../components/PopularProducts/PopularProducts";
import PopularCategory from "../components/PopularCategory/PopularCategory";
// import '../../../styles/scss/basic-style.scss'
import BannerSlider from "../components/bannerSlider/components/BannerSlider/BannerSlider";
import '../../../components/Preloader/preloader.scss'


const HomePage = () => {
    const loading = useSelector(state => state.allProduct.loading);
    const preloadingStyle = loading ? {display: 'block'} : {display: 'none'};
    return(
        <>
            <Preloader style={preloadingStyle} classNameContainer='containerPreloader' classNameElement='preloader-5'/>
            <section className='container'>
                <BannerSlider />
                <PopularProducts titleText='Популярные товары' />
                <PopularCategory />
            </section>
        </>
    )
}

export default HomePage
