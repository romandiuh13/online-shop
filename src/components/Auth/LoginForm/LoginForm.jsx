import React, {useState,} from 'react';
import { useHistory } from 'react-router-dom'
import Input from "../../Input/Input";
import axios from "axios";
import {fields} from "./loginFormFields";
import setAuthToken from "../setAuthToken/setAuthToken";
import './loginForm.scss'
import {useDispatch} from "react-redux";
import {addNewUser} from "../../../app/store/Auth/actions/addNewuser";
import {closeModal} from "../../../app/store/Modal/actions/closeModal";

const LoginForm = ({style}) =>{
    const dispatch = useDispatch();
    const [inputValues, setInputValues] = useState({})
    const history = useHistory()

    const handleOnChange = event => {
        const { name, value } = event.target;
        inputValues[name] = value
        setInputValues(inputValues);
    };
    const handleSubmit = (event)=>{
        event.preventDefault();
        const sendRequest = async () =>{
            try {
                const resp = await axios.post('https://glacial-river-19645.herokuapp.com/api/customers/login', inputValues,
                    {headers: {'Content-Type': 'application/json' }} )
                setAuthToken(resp.data.token)
                localStorage.setItem('token', JSON.stringify(resp.data.token))
                const customer = await axios.get('https://glacial-river-19645.herokuapp.com/api/customers/customer')
                setInputValues({})
                dispatch(addNewUser(customer.data))
                localStorage.setItem('customer', JSON.stringify(customer.data))
                dispatch(closeModal())
                history.push('/userProfile')
            } catch (error){

                alert(error.response.data.loginOrEmail)
                console.dir(error)
            }
        }
        sendRequest()
    };
        return (
                <form style={style} className='container-SingIn'  onSubmit={handleSubmit} >
                    <Input className='loginForm-container' {...fields.loginOrEmail} onChange = {handleOnChange}/>
                    <Input className='loginForm-container' {...fields.password} onChange = {handleOnChange}/>
                    <Input className='buttonAuth' {...fields.submit}/>
                </form>

        );

}

export default LoginForm;
