export const fields ={

    firstName: {
        classNameLabel: 'authLabel',
        type: 'text',
        placeholder: 'firstName',
        cName: 'registerForm-container',
        name: 'firstName',
        label: "FirstName",
        text: 'firstName',
        minLength: 2,
        maxLength: 25,
        required: true

    },
    lastName: {
        classNameLabel: 'authLabel',
        type: 'text',
        placeholder: 'lastName',
        cName: 'registerForm-container',
        name: 'lastName',
        label: 'lastName',
        text: 'lastName',
        minLength: 2,
        maxLength: 16,
        required: true
    },
    login: {
        classNameLabel: 'authLabel',
        type: 'text',
        placeholder: 'Login',
        cName: 'registerForm-container',
        name: 'login',
        label: 'Login',
        text: 'Login логин',
        minLength: 3,
        maxLength: 10,
        required: true
    },
    email: {
        classNameLabel: 'authLabel',
        type: 'email',
        placeholder: 'email',
        cName: 'registerForm-container',
        name: 'email',
        label: 'Email',
        text: 'Email',
        required: true
    },
    password: {
        classNameLabel: 'authLabel',
        type: 'password',
        placeholder: 'Пароль',
        cName: 'registerForm-container',
        name: 'password',
        label: 'password',
        text: 'Придумайте пароль',
        minLength: 7,
        maxLength: 30,
        required: true
    },
    submit: {
        type: 'submit',
        value: 'Регистрация',
        name: 'submit',
        cName: 'submit-btn'
    },


}





