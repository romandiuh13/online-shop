import React from 'react';
import './Footer.scss'
import Logo from "../Logo/Logo";
import logoFooter from "../../assets/logo/LORI_LOGO2.png";
import {NavLink} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faArrowRight,
    faCaretSquareRight,
    faEnvelope
} from "@fortawesome/free-solid-svg-icons";

import {faFacebookSquare, faInstagramSquare} from "@fortawesome/free-brands-svg-icons";

const Footer = () => {
    return (
        <div className={'footer'}>
            <div className={'footer-main'}>
                <div className={'footer-main-label'}>
                    <div className={'footer-main-label-logo'}>
                        <Logo src={logoFooter} classNames='footer-main-logo-item' flag={false}/>
                    </div>
                    <div className={'footer-main-label-description'}>
                        <span>Фабрика «LORI» – украинский изготовитель,европейськое качество!</span></div>
                </div>
                <div className={'footer-main-info'}>
                    <span className={'footer-main-info-heading'}>Информация</span>
                        <NavLink className={'footer-main-info-link'} to={'#'}><span>О нас</span></NavLink>
                        <NavLink className={'footer-main-info-link'} to={'#'}><span>Партнерам</span></NavLink>
                        <NavLink className={'footer-main-info-link'} to={'#'}><span>Вакансии</span></NavLink>
                </div>
                <div className={'footer-main-buyers'}>
                    <span className={'footer-main-buyers-heading'}>Покупателям</span>
                    <NavLink className={'footer-main-buyers-link'} to={'#'}><span>Гарантия</span></NavLink>
                    <NavLink className={'footer-main-buyers-link'} to={'#'}><span>Оплата та доставка</span></NavLink>
                    <NavLink className={'footer-main-buyers-link'} to={'#'}><span>Обмен та Возврат</span></NavLink>
                    <NavLink className={'footer-main-buyers-link'} to={'#'}><span>Кредит та оплата частинями</span></NavLink>
                </div>
                <div className={'footer-main-contacts'}>
                    <span className={'footer-main-contacts-heading'}>Контакти</span>
                    <span className={'footer-main-contacts-place'}>м. Полтава, ул. Освобождения, 26 <a href="#" className={'footer-main-contacts-place'}>info@lori.com.ua</a></span>

                </div>
                <div className={'footer-main-subscription'}>
                    <a href={'#'} className={'footer-main-subscription-heading'}>Подписка на розсылку&nbsp;
                    <FontAwesomeIcon className='footer-main-subscription-heading-icon' icon={faEnvelope}/></a>
                    <label htmlFor="" className='footer-main-subscription-label'>
                        <input className='footer-main-subscription-label-input' type="text"/>
                        <a href="#"><FontAwesomeIcon icon={faCaretSquareRight} className='footer-main-subscription-label-icon'/></a>
                        </label>
                    <div className={'footer-main-subscription-media'}>
                        <span>Следите за нами</span>&nbsp;
                        <a href="#"><FontAwesomeIcon className='footer-main-subscription-media-icon' icon={faFacebookSquare}/></a>&nbsp;
                        <a href="#"><FontAwesomeIcon className='footer-main-subscription-media-icon' icon={faInstagramSquare}/></a>
                    </div>
                </div>
            </div>
            <hr className={'footer-decoration-line'}/>
            <p className={'footer-copyright'}>&copy; 1997-2020 Меблева фабрика «LORI». Все права захищищены. </p>

        </div>
    );
};

export default Footer;
