import React from "react";
import './modalList-style.scss'
import {category} from "../../../HeaderCategory/category";
import {NavLink} from "react-router-dom";
import {v4 as uuidv4} from "uuid";

const ModalList = ({classNames, close}) =>{
    const listCategory = category.map(({name, productUrl}) => <li key={uuidv4()} onClick={close} className={classNames}><NavLink  className='link' to={`${productUrl}`} >{name}</NavLink></li>)

    return(
        <div className='category-productsModal'>
            {listCategory}
        </div>

    )
}
export default ModalList
