import React from "react";
import {linkProps} from "./linkProps";
import {NavLink} from "react-router-dom";
import {v4 as uuidv4} from 'uuid'
import '../../../Header/header-style.scss'

const Navbar = ({className, close}) => {
    const linksElements = linkProps.map(({to, text}) =>
        <li onClick={close} key={uuidv4()} className={className}>
            <NavLink to={to}  className='link' activeClassName="link--active">{text}</NavLink>
        </li>)
    return (
             <>
               {linksElements}
             </>)
}

export default Navbar
