export const linkProps = [
    {
        to: '/Home',
        text: 'Главная'
    },
    {
        to: '/Partners',
        text: 'Партнерам'
    },
    {
        to: '/Shops',
        text: 'Наши магазины'
    },
    {
        to: '/contacts',
        text: 'Про нас'
    },
    {
        to: '/shares',
        text: 'Акции'
    }

]
