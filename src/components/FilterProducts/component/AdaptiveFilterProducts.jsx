import React from "react";
import {fieldsColor} from "../fields";
import {useDispatch} from "react-redux";
import {actionFilterProducts} from "../../../app/store/Products/actions/actionFilterProducts";
import {clearFilter} from "../../../app/store/Products/actions/clearFilter";

const AdaptiveFilterProducts = ({className}) =>{
    const dispatch = useDispatch();
    const result = fieldsColor.map(({type, name, value,text}) => {
        return(<option  value={value}>{text}</option>)
    })

    return(
        <div className={className}>
            <select  onChange={(event => dispatch(actionFilterProducts(event.target.value)))} id="">
                <option onClick={() => dispatch(clearFilter([]))} >Выбирите цвет / очистить фильтр</option>
                {result}
            </select>
        </div>
    )
}
export default AdaptiveFilterProducts
