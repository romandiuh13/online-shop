import React from 'react';
import './Thank.scss'
import {NavLink} from "react-router-dom";
import Button from "../Button";
import {useDispatch} from "react-redux";
import {clearCart} from "../../app/store/Cart/actions/clearCartAction";
import {closeModal} from "../../app/store/Modal/actions/closeModal";

const Thank = () => {
    const dispatch = useDispatch()
    const finalClick = () => {
        dispatch(clearCart());
        dispatch(closeModal())
    }
    return (
        <div className={'thank'}>
            <h3 className={'thank-heading'}>Дякуємо, що вибрали нас!</h3>
            <p className={'thank-main'}>Ваше замовлення №3265897 успішно оформлене.
                Чекайте на дзвінок від нашого фахівця.  </p>
            <NavLink to={'/'}><Button className={'thank-btn'} onClick={()=>finalClick()} text={'продовжити покупки'}/></NavLink>
        </div>
    );
};

export default Thank;
