import React  from 'react';
import {v4 as uuid} from 'uuid'
import PropTypes from 'prop-types';

const Input = (props) => {
    const {htmlFor,text, cName, type, placeholder, name, onChange, error, classNames, classNameLabel, ...inputProps} = props

        return (
            <div className={classNames} key={uuid()} >
                <label className={classNameLabel} htmlFor={htmlFor}>{text}</label>
                <input className={cName} type={type}  name={name} placeholder={placeholder}
                       onChange={onChange} {...inputProps} />
                {error && (<p className="error">{error}</p>
                )}
            </div>
        );
}

Input.propTypes = {
    name: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
}



export default Input;
