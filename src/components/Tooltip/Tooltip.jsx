import React,{useState, useRef}  from 'react';
import Overlay from 'react-bootstrap/Overlay'
import Button from 'react-bootstrap/Button'

const Tooltip = () => {
    const [show, setShow] = useState(false);
    const target = useRef(null);
    return (
        <>
            <Button variant="danger" ref={target}
                    onClick={() => setShow(!show)}>
                У кошик
            </Button>
            <Overlay target={target.current} show={show} placement="right">
                {({placement, arrowProps, show: _show, popper, ...props}) => (
                    <div
                        {...props}
                        style={{
                            backgroundColor: 'rgb(87 97 91)',
                            padding: '2px 10px',
                            color: 'white',
                            borderRadius: 3,
                            ...props.style,
                        }}
                    >
                        Товар доданий
                    </div>
                )}
            </Overlay>
        </>
    );
};

export default Tooltip;
