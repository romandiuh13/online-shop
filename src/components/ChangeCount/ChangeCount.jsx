import React from 'react';
import './ChangeCount.scss'

const ChangeCount = ({classNameContainer, onClickMinus, onClickPlus, count}) => {
    return (
        <div className={`change-count ${classNameContainer}`}>
            <span className={'change-count-minus'} onClick={onClickMinus}>-</span>
            <span className="change-count-text">шт</span>
            <input type="number" min={'0'} value={count} defaultValue={'1'} className={'change-count-input'} step="0.01"/>
            <span className={'change-count-plus'} onClick={onClickPlus}>+</span>
        </div>
    )
}

export default ChangeCount;
