import React from "react";
import {v4 as uuidv4} from "uuid";

const Form = ({fields, className}) => {
    const form = fields.map(({name, type}) => (
        <>
            <span className='form-heading'>{name}</span>
            <input className='form-field' type={type}/>
        </>
    ))
    return (
        <form key={uuidv4()} className={className}>
            {form}
        </form>
    )
}

export default Form
