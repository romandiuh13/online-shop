import React from "react";
import './preloader.scss'

const Preloader = ({style, classNameContainer, classNameElement}) =>{
    return(
        <div style={style} className={classNameContainer}>
            <div style={style} className={classNameElement}></div>
        </div>


    )
};

export default Preloader
