import {OPEN_MODAL} from "../constants";

export const openModal = (payload) => {
    return{
        type: OPEN_MODAL,
        payload
    }
}
