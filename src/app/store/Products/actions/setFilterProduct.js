import {FILTER_COLOR} from "./actionType";

export const setFilterProduct = (payload) =>{
    return{
        type: FILTER_COLOR,
        payload
    }
};
