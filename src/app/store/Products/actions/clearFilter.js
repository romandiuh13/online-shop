import {CLEAR_FILTER} from "./actionType";

export const clearFilter = (payload) =>{
    return{
        type: CLEAR_FILTER,
        payload
    }
};