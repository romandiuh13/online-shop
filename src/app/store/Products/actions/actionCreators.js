import axios from "axios";
import {FETCH_PRODUCTS_REQUEST, FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCTS_FAILED} from './actionType'

export const productsRequestCreator = (page, limit) => {
     const func  = async (dispatch) =>{
         dispatch({type: FETCH_PRODUCTS_REQUEST})
         try{
             // const fullURL = `${url}?page=${page}&limit=${limit}`
             const response = await axios.get('https://glacial-river-19645.herokuapp.com/api/products/')
             const {data} = response;

             dispatch({type: FETCH_PRODUCTS_SUCCESS, payload: data})
         } catch (error) {
             dispatch({type: FETCH_PRODUCTS_FAILED, payload: error})
         }
     }
     return func;
 }

