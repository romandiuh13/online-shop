import axios from "axios";
import { FETCH_PRODUCTS_REQUEST, FILTER_COLOR, FETCH_PRODUCTS_FAILED} from "./actionType";

export const actionFilterProducts = (color, category) =>{
    const func  = async (dispatch) =>{
        dispatch({type: FETCH_PRODUCTS_REQUEST})
        try{
            const response = await axios.get(`https://glacial-river-19645.herokuapp.com/api/products/filter?color=${color}`)
            const {data} = response;
            console.log(data)
            dispatch({type: 'ADD', payload: data.products})
        } catch (error) {
            dispatch({type: FETCH_PRODUCTS_FAILED, payload: error})
        }
    }
    return func;
}