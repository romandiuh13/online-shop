import {CHANGE_NAV} from "./constants";

const initialState = {
    isOpen: false
}

export const headerReducer = (state = initialState, {type}) => {
    switch (type) {
        case CHANGE_NAV:
            return {...state, isOpen: !state.isOpen}
        default:
            return state
    }
}
