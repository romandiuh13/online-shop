export const ADD_CART = 'ADD_CART';
export const REMOVE_CART = 'REMOVE_CART';
export const CLEAR_CART = 'CLEAR_CART'
export const MINUS_ITEM = 'MINUS_ITEM';
export const PLUS_ITEM = 'PLUS_ITEM';
export const GET_TOTAL_PRICE = 'GET_TOTAL_PRICE';
export const ADD_FROM_LOCALSTORAGE = 'ADD_FROM_LOCALSTORAGE';

