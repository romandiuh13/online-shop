import {ADD_FROM_LOCALSTORAGE, PLUS_ITEM} from "../constants";

export const addFromLocalStorage = (payload)=>{
    return{
        type: ADD_FROM_LOCALSTORAGE,
        payload,
    }
}
