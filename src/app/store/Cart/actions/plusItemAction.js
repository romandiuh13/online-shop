import {PLUS_ITEM} from "../constants";

export const plusItem = (payload) =>{
    return {
        type: PLUS_ITEM,
        payload
    }
}
